#include <stdio.h>
#include <limits.h>
#include <assert.h>

unsigned int mul7( unsigned int n )
{
	#if 0 /* Uses Addition */
	// 7n = (2^2)n + (2^1)n + (2^0)n
	return (n << 2) + (n << 1) + n;
	#else /* Uses Subtraction */
	// 7n = (2^3)n - (2^0)n
	return (n << 3) - (n << 0);
	#endif
}

unsigned int div7__6( unsigned int n )
{
	// n / 7 ~= n * (9 / 63)
	// 9n = (2^3)n + (2^0)n
	// 63 ~= 2^6
	n = n + 1;
	n = (n << 3) + (n << 0);
	n = (n >> 6);
	return n;
}

unsigned char div7__8( unsigned char n )
{
	// n / 7 ~= n * (37 / 256)
	// 37n = (2^5)n + (2^2)n + (2^0)n
	// 256 ~= 2^8
	unsigned short q = n;
	q = (q << 5) + (q << 2) + (q << 0);
	q = (q >> 8);

	unsigned char t = (q << 3) - q;

	if( t > n )
	{
		q -= 1;
	}

	return q;
}

unsigned int div7__9( unsigned int n )
{
	// n / 7 ~= n * (73 / 511)
	// 73n = (2^6)n + (2^3)n + (2^0)n
	// 511 ~= 2^9
	n = n + 1;
	n = (n << 6) + (n << 3) + (n << 0);
	n = (n >> 9);
	return n;
}

unsigned int div7__32( unsigned int n )
{
	// n / 7 ~= n * (613566757 / 4294967296)

	// 613566757n = (2^29)n    + (2^26)n   + (2^23)n  + (2^20)n  + (2^17)n + (2^14)n + (2^11)n + (2^8)n + (2^5)n + (2^2)n + (2^0)n
	//            = 536870912n + 67108864n + 8388608n + 1048576n + 131072n + 16384n  + 2048n   + 256n   + 32n    +  4n    + 1
	//
	// 4294967296 = 2^32

	unsigned long q = n;
	q = (q << 29) + (q << 26) + (q << 23) + (q << 20) + (q << 17) +
	    (q << 14) + (q << 11) + (q << 8) + (q << 5) + (q << 2) + (q << 0);

	q = q >> 32;
	return q;
}

int main( int argc, char* argv[] )
{
	unsigned int q = argc;

#if 1
	unsigned int (*div_fxn)( unsigned int n ) = div7__32;

	for( unsigned int i = 7; i <= 10000000; i += 7 )
	{
		printf( "%2u / 7 = %3u\n", i-1, div_fxn(i-1) );
		assert( div_fxn(i-1) == ((i - 1)/7) );
		printf( "%2u / 7 = %3u\n", i, div_fxn(i) );
		assert( div_fxn(i) == (i/7) );
		printf( "%2u / 7 = %3u\n\n", i+1, div_fxn(i+1) );
		assert( div_fxn(i+1) == ((i+1)/7) );
	}
#else
	unsigned char (*div_fxn)( unsigned char n ) = div7__8;

	for( unsigned int i = 1; i < UCHAR_MAX; i++ )
	{
		printf( "%2u / 7 = %3u\n", i-1, div_fxn(i-1) );
		assert( div_fxn(i-1) == ((i - 1)/7) );
		printf( "%2u / 7 = %3u\n", i, div_fxn(i) );
		assert( div_fxn(i) == (i/7) );
		printf( "%2u / 7 = %3u\n\n", i+1, div_fxn(i+1) );
		assert( div_fxn(i+1) == ((i+1)/7) );
	}
#endif

	return 0;
}
